import axios from 'axios';
import { throttleAdapterEnhancer } from 'axios-extensions';
import store from '../store'

const throttleConfig = {
    threshold: 2*1000 // 2 seconds
} 

const httpClient = axios.create({
    baseURL: "http://127.0.0.1/api/",
    timeout: 10000,
    adapter: throttleAdapterEnhancer(axios.defaults.adapter, throttleConfig)
});

const errorInterceptor = error => {
    if(error.response){
        
        switch(error.response.status) {
            case 400:
                console.error(error, error.message);
                break;
            case 401: 
                store.commit('user/saveToken', null)
                break;
    
            default:
                console.error(error, error.message);
    
        }
    }
    return Promise.reject(error);
}

const responseInterceptor = response => {
    switch(response.status) {
        case 200: 
            // yay!
            break;
        // any other cases
        default:
            // default case
    }

    return response;
}

httpClient.interceptors.response.use(
    responseInterceptor, 
    errorInterceptor);

export default httpClient;