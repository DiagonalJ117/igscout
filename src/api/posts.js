import httpClient from './httpClient';

const ENDPOINT = "https://www.instagram.com/p/";
const GQLSUFFIX = "?__a=1";

const getPost = (postcode) => httpClient.get(ENDPOINT+postcode+GQLSUFFIX);

export default {
    getPost
}