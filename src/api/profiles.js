import httpClient from './httpClient';

const ENDPOINT = "https://www.instagram.com/";
const GQLSUFFIX = "?__a=1";

const getProfile = (username) => httpClient.get(ENDPOINT+username+GQLSUFFIX);

export default {
    getProfile
}