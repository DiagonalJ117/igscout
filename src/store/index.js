import Vue from 'vue'
import Vuex from 'vuex'
import preferences from './modules/preferences'

Vue.use(Vuex)

const debug = true

export default new Vuex.Store({
    modules: {
        preferences
    },
    strict: debug,
    plugins: []
})