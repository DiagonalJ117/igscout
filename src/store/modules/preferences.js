const state = {
    theme: localStorage.getItem('theme')
}

const getters = {
    theme: (state) => {
        return state.theme
    }
}

const actions = {

}

const mutations = {
    initializeTheme(state) {
        let theme = localStorage.getItem('theme')
        console.log(theme);
        if (theme) {
            state.theme = theme;
        }
    },
    changeTheme(state) {
        state.theme = state.theme === "light" ? "dark" : "light"
        localStorage.setItem("theme", state.theme)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}