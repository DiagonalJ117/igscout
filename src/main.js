import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import store from './store'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import router from './router'

Vue.use(VueRouter)
Vue.config.productionTip = false

import SideBar from './components/dashboard/SideBar'
import TopBar from './components/dashboard/TopBar'
import Footer from './components/dashboard/Footer'
import SearchBar from './components/SearchBar'
import ProfileCard from './components/ProfileCard'
import PostCard from './components/PostCard'

Vue.component('side-bar', SideBar)
Vue.component('top-bar', TopBar)
Vue.component('footer-bar', Footer)
Vue.component('search-bar', SearchBar)
Vue.component('profile-card', ProfileCard)
Vue.component('post-card', PostCard)

new Vue({
  vuetify,
  router,
  store,
  beforeCreate() {
    this.$vuetify.theme.dark = this.$store.state.preferences.theme !== "light"
  },
  render: h => h(App)
}).$mount('#app')
